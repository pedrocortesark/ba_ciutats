﻿using System;
using System.Text;

namespace BA_CiutatsFase3
{
    class Program
    {
        static void Main(string[] args)
        {
            string city1 = "", city2 = "", city3 = "", city4 = "", city5 = "", city6 = "";

            Console.WriteLine("Bienvenido al mundo de las ciudades!");
            Console.WriteLine("Por favor, introduce el nombre de 6 ciudades");

            for (int i = 1; i <= 6; i++)
            {
                Console.WriteLine($"Introduce el nombre de la ciudad {i}");

                if (i == 1)
                    city1 = Console.ReadLine();
                else if (i == 2)
                    city2 = Console.ReadLine();
                else if (i == 3)
                    city3 = Console.ReadLine();
                else if (i == 4)
                    city4 = Console.ReadLine();
                else if (i == 5)
                    city5 = Console.ReadLine();
                else
                    city6 = Console.ReadLine();
            }

            Console.WriteLine($"\r\nLas ciudades que has introducido son las siguientes: {city1}, {city2}, {city3}, {city4}, {city5} y {city6}");

            string[] arrayCiutats = new string[6] { city1, city2, city3, city4, city5, city6 };

            Array.Sort(arrayCiutats);

            Console.WriteLine("\r\nPulse Enter para conocer la lista ordenada de ciudades introducidas...");
            Console.ReadLine();

            foreach (string ciutat in arrayCiutats)
            {
                Console.WriteLine(ciutat);
            }

            string[] arrayCiutatsModificades = new string[6];

            for (int i = 0; i < arrayCiutatsModificades.Length; i++)
            {
                string newcity = arrayCiutats[i].ToLower().Replace("a", "4");
                arrayCiutatsModificades[i] = newcity;
            }

            Array.Sort(arrayCiutatsModificades);

            Console.WriteLine("\r\nPulse Enter para conocer la lista ordenada de ciudades modificadas con nuestro programa...");
            Console.ReadLine();

            foreach (string ciutat in arrayCiutatsModificades)
            {
                Console.WriteLine(ciutat);
            }

            
            char[][] lastArrayEver = new char[arrayCiutats.Length][];

            for (int i = 0; i < arrayCiutats.Length; i++)
            {
                lastArrayEver[i] = new char[arrayCiutats[i].Length];

                for (int j = 0; j < arrayCiutats[i].Length; j++)
                {
                    lastArrayEver[i][j] = arrayCiutats[i][j];
                }
            }
            Console.WriteLine("\r\nPulse Enter para conocer nuestro último truco...");
            Console.ReadLine();

            for (int i = 0; i < arrayCiutats.Length; i++)
            {
                var builder = new StringBuilder();
                Array.Reverse(lastArrayEver[i]);

                foreach (var lletre in lastArrayEver[i])
                {
                    builder.Append(lletre);
                }

                string newCity = arrayCiutats[i] + " - " + builder.ToString();
                Console.WriteLine(newCity);

            }
        }
    }
}
